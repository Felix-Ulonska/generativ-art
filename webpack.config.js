const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.ts',
	mode: 'development',
  module: {
    rules: [
			{
				test: /\.(s(a|c)ss)$/,
			  use: ['style-loader','css-loader', 'sass-loader']
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      }
    ],
  },
	devtool: 'inline-source-map',
	 plugins: [
	 new HtmlWebpackPlugin({
		 title: 'Development',
		 template: 'src/index.html'
	 }),
 ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
